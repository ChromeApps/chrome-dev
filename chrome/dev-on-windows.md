# Windows Chrome 开发环境

## 扩展数据目录

`C:\Users\XXX\AppData\Local\Google\Chrome\User Data\Default\Extensions`

```cmd
set CHROME_EXTENSIONS=%LOCALAPPDATA%\Google\Chrome\User Data\Default\Extensions
echo %CHROME_EXTENSIONS%
explorer %CHROME_EXTENSIONS%
```

提示：使用 `set` 命令查看 Windows 环境变量。和 Linux 上的 `export` 一样，接参数表示设置变量，不接参数就是查看所有变量。

