# Chrome 浏览器

## Chrome URLs

都在 <chrome://chrome-urls>（别名：<chrome://about>）里面。

#### 常用

功能 | URL
---- | ---
书签 | <chrome://bookmarks>
历史 | <chrome://history>
下载 | <chrome://downloads>
应用 | <chrome://apps>
扩展程序 | <chrome://extensions>
设置 | <chrome://settings>
打开新标签页 | <chrome://newtab>
关于 | <chrome://help>、<chrome://chrome>

#### 开发相关

功能 | URL
---- | ---
版本 | <chrome://version>
调试 | <chrome://profiler>
内存 | <chrome://memory-redirect>
内存 | <chrome://memory-internals>
IndexedDB | <chrome://indexeddb-internals>
实验性功能 | <chrome://flags>
？ | <chrome://accessibility>
？ | <chrome://local-state>
插件 | <chrome://plugins>
组件 | <chrome://components>
加载到主进程中的模块 | <chrome://conflicts>
DNS | <chrome://dns>
缓存 | <chrome://cache>
应用缓存 | <chrome://appcache-internals>

## 参考

- 百度经验，[Chrome 高级技巧](http://jingyan.baidu.com/season/15467)

